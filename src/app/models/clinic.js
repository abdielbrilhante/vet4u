import Backbone from 'backbone';

export const Clinics = Backbone.Model.extend({
  urlRoot: 'http://localhost:3001/clinics',
});

export default new Clinics();
