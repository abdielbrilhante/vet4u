import Marionette from 'backbone.marionette';

import template from './List.hbs';

function search(obj, query) {
  if (!obj) {
    return false;
  } else if (typeof obj === 'object') {
    return !!Object.values(obj).find(item => search(item, query));
  } else if (['string', 'number'].includes(typeof obj)) {
    return String(obj).toLowerCase().includes(query.toLowerCase());
  }

  return false;
}

export default Marionette.View.extend({
  template,

  serializeData() {
    const { items, filter } = this.options;
    return {
      items: items.filter(item => search(item, filter)),
    };
  },
});
