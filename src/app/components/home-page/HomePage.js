import Marionette from 'backbone.marionette';

import template from './HomePage.hbs';
import collection from '../../collections/clinic';
import List from '../list-region/List';

export default Marionette.View.extend({
  template,
  collection,

  events: {
    'keyup #filter': 'filter',
  },

  regions: {
    list: '#list',
  },

  initialize() {
    this.collection.fetch();
    this.listenTo(this.collection, 'sync', this.render);
  },

  onRender() {
    this.showList();
  },

  filter(e) {
    this.showList(e.target.value);
  },

  showList(filter = '') {
    this.showChildView(
      'list',
      new List({ items: this.collection.toJSON(), filter }),
    );
  },
});
