import Marionette from 'backbone.marionette';
import { getDay, parse, isAfter, isBefore } from 'date-fns';

import template from './DetailsPage.hbs';
import model from '../../models/clinic';
import { weekday } from '../../util/weekday';

export default Marionette.View.extend({
  template,
  model,

  initialize() {
    this.model.set({ id: this.id }).fetch();
    this.listenTo(this.model, 'sync', this.render);
  },

  serializeModel() {
    const model = this.model.toJSON();
    const { workingHours = [] } = model;

    const now = new Date();
    const today = workingHours[getDay(now)];

    let isOpen = false;
    if (today && today.begin && today.end) {
      const begin = parse(today.begin, 'HH:mm', now);
      const end = parse(today.end, 'HH:mm', now);

      isOpen = isAfter(now, begin) && isBefore(now, end);
    }

    return {
      ...model,
      isOpen,
      workingHours: workingHours.map((pair, i) => ({
        ...pair,
        label: weekday(i),
      })),
    };
  },
});
