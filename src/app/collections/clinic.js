import Backbone from 'backbone';

export const Clinics = Backbone.Collection.extend({
  url: 'http://localhost:3001/clinics',
});

export default new Clinics();
