import $ from 'jquery';
import Backbone from 'backbone';

import HomePage from 'app/components/home-page/HomePage';
import DetailsPage from 'app/components/details-page/DetailsPage';

export default Backbone.Router.extend({
  routes: {
    '': 'home',
    ':id': 'details',
  },

  home() {
    const homePage = new HomePage().render();
    $('#root').empty().append(homePage.$el);
  },

  details(id) {
    const detailsPage = new DetailsPage({ id }).render();
    $('#root').empty().append(detailsPage.$el);
  },
});

