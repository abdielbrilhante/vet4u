const express = require('express');
const app = express();
const cors = require('cors');
const server = require('http').Server(app);
const faker = require('faker/locale/pt_BR');
const PORT = process.env.PORT || 3001;

faker.locale = 'pt_BR';

const prefixes = ['Clínica ', 'Hospital '];
const suffixes = ['clinic', ' Pet Shop', ' Clínica Veterinária'];

const randomPick = options =>
	options[Math.floor(Math.random() * options.length)];
const clinicName = name => Math.random() < 0.5
	? randomPick(prefixes) + name
	: name + randomPick(suffixes);

const specialties = [
	'Animais exóticos',
	'Animais selvagens',
	'Geral',
	'Animais de estimação',
];

const buildClinic = id => {
	const name = clinicName(faker.name.lastName());
	return {
		id,
		name,
		description: faker.lorem.paragraph(),
		descriptionSub: faker.lorem.paragraph(),
		specialty: randomPick(specialties),
		location: {
			address: faker.address.streetAddress(),
			zipCode: faker.address.zipCode(),
			city: faker.address.city(),
		},
		contact: {
			phone: faker.phone.phoneNumber().replace(/\+\d+\s/, ''),
			email: faker.internet.email().toLowerCase(),
			website: 'www.' + faker.internet.url().replace(/\w+:\/\//, ''),
		},
		workingHours: [
			null,
			{ "begin": "08:00", "end": "19:00" },
			{ "begin": "08:00", "end": "19:00" },
			{ "begin": "08:00", "end": "19:00" },
			{ "begin": "08:00", "end": "19:00" },
			{ "begin": "08:00", "end": "19:00" },
			null
		],
	};
};

app.use(cors());
app.use(express.json());

const db = Array(40).fill(null).map((item, i) => buildClinic(i + 1))

app.get('/clinics', (req, res) => {
  res.send(JSON.stringify(db));
});

app.get('/clinics/:id', (req, res) => {
  res.send(JSON.stringify(db[req.params.id - 1]));
});

server.listen(PORT);
