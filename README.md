## Instalação

```bash
yarn # ou npm install
```

## Executar

Para iniciar o servidor (Node):
```bash
yarn server # ou npm run server
```

Para iniciar o cliente, em outro terminal, executar o dev server do Webpack:
```bash
yarn dev # ou npm run dev
```

A página estará disponível em [localhost:3000](http://localhost:3000).

## Estrutura

O projeto utiliza como base um [boilerplate](https://github.com/sabarasaba/modern-backbone-starterkit/) para Backbone com Webpack. O boilerplate inclui [Marionette.js](https://marionettejs.com/) e utiliza [Handlebars.js](https://handlebarsjs.com/) como engine de template. Adicionalmente, o framework CSS [Milligram](https://milligram.io/) foi utilizado (primariamente para tipografia e tabelas).

O servidor usa [faker.js](https://www.npmjs.com/package/faker) para a geração dos dados *mockados* retornados.
